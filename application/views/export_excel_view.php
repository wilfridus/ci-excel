<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Export Data To Excel</title>
  </head>
  <body>
    <?php
    	header("Content-type: application/vnd-ms-excel");
    	header("Content-Disposition: attachment; filename=Data Employee.xls");
      if ($sExport == "excel") {
      	header('Content-Type: application/vnd.ms-excel');
      	header('Content-Disposition: attachment; filename=' . EW_REPORT_TABLE_VAR .'.xls');
      }
      if ($sExport == "word") {
      	header('Content-Type: application/vnd.ms-word');
      	header('Content-Disposition: attachment; filename=' . EW_REPORT_TABLE_VAR .'.doc');
      }
  	?>
    <h3>Export Data to Excel in Codeiniter using PHPExcel</h3>
    <table class="table table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">NIK</th>
          <th scope="col">NAMA</th>
          <th scope="col">Tanggal Lahir</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1; foreach ($employee_data as $row) {?>
        <tr>
          <th scope="row"><?= $i++ ?></th>
          <td><?= $row->nik ?></td>
          <td><?= $row->nama ?></td>
          <td><?= $row->tgl_lahir ?></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
<div class="" align="center">
  <form  method="post" action="<?= base_url('excel_export/action'); ?>">
    <input type="submit" name="export" value="Export Excel" class="btn btn-success">
  </form>
</div>
<a href="<?= base_url('excel_export'); ?>"></a>
  </body>
</html>
