<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Excel_export extends CI_Controller
{

  public function index() {
    $this->load->model('export_excel_model');
    $data['employee_data'] = $this->export_excel_model->fetch_data();
    $this->load->view('export_excel_view', $data);
  }

  public function action() {
    $this->load->model('export_excel_model');
    $this->load->library('excel');
    $object = new PHPExcel();

    $object->setActiveSheetIndex(0);
    $table_columns = array('NIK','NAMA', 'Tanggal Lahir');
    $column = 0;

    foreach ($table_columns as $field) {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
      $column++;
    }
    $employee_data = $this->export_excel_model->fetch_data();

    $excel_row = 2;

    foreach ($employee_data as $row) {
      $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->nik);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->nama);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->tgl_lahir);
      $excel_row++;
    }

    $object_writer = PHPExcel_IOFactory::createwriter($object,'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename=EmployeeData.xls');
    $object_writer->save('php://output');
  }

}
